import React, { Component } from 'react'
import "./AllProducts.css"
import ProductsDetails from './ProductsDetails'

export class AllProducts extends Component {

    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        }
        this.state = {
            products: [],
            status: this.API_STATES.LOADING,
            errorMessage: "",
        }

        this.URL = 'https://fakestoreapi.com/products';

    }
    fetchData = (url) => {

        this.setState({
            status: this.API_STATES.LOADING,
        }, () => {

            fetch(url).then(data => {
                return data.json()
            }).then(data => {
                console.log(data)
                this.setState({
                    status: this.API_STATES.LOADED,
                    products: data
                })
            }).catch(error => {
                this.setState({
                    status: this.API_STATES.ERROR,
                    errorMessage: "An API error occurred. Please try again in a few minutes."
                })
            })
        })
    }

    componentDidMount = () => {

        this.fetchData(this.URL)
    }
    reloadData = () => {
        this.fetchData(this.URL)
    }
    render() {
        let products = this.state.products
        let alldata = products.map(data => {
            return <ProductsDetails data={data} />
        })
        console.log(alldata)
        return (
            <>
                {/* <button onClick={this.reloadData}>Reload Data</button> */}
                {this.state.status === this.API_STATES.LOADING && <div className="loader">Loading..</div>}

                {this.state.status === this.API_STATES.ERROR &&
                    <div className='error'>
                        <h1>{this.state.errorMessage}</h1>
                    </div>
                }
                {this.state.status === this.API_STATES.LOADED && products.length === 0 &&
                    <div className='error'>
                        <h1>No products available at the moment. Please try again later.</h1>
                    </div>
                }
                {<div className='All-data-container'>{alldata}</div>}

            </>
            // this.state.isLoading ? (
            //     <div className="loading-continer">Loading...</div>
            // ) : (<>
            //     <div className='check-1'>{alldata}</div>

            // </>)


        )
    }
}

export default AllProducts
