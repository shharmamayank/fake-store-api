import React, { Component } from 'react'
import "./ProductDetails.css"
export class ProductsDetails extends Component {
    render() {
        console.log(this.props.data)
        return (
            <>
                <div className='check'>
                    <div className=' image-div'>
                        <img className='img-container' src={(this.props.data.image)} alt="" />
                    </div>
                    <div className='text-details'>
                        <div className='category'><b>Category : &nbsp;</b><p>{(this.props.data.category
                        )}</p></div>
                        <div>
                            <span><b>Title:&nbsp;</b>{(this.props.data.title)}</span>
                        </div>
                        <div>
                            <span><b>Price:</b> &nbsp;{(this.props.data.price)}</span>
                        </div>
                        <div>
                            <span><b>Count: &nbsp;</b>{(this.props.data.rating.count)}&nbsp; <b>Rate: &nbsp;</b> {(this.props.data.rating.rate)}</span>
                        </div>
                        <span className='description'><b>Description:</b> &nbsp;{(this.props.data.description)}</span>
                       
                    </div>
                </div>


            </>
        )
    }
}

export default ProductsDetails
