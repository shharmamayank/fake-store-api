import './App.css';
import AllProducts from './Component/AllProducts';
import Navbar from './Component/Navbar';

function App() {
  return (
    <>
      <Navbar />
      <AllProducts />
    </>
  );
}

export default App;
